/// DownloadPage
pub mod download_page;
/// VideoPage
pub mod video_page;

pub use self::{
    download_page::DownloadPage,
    video_page::VideoPage,
};
use url::Url;

/// Data about stream sources
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct DownloadData {
    /// Captions?
    pub captions: Vec<serde_json::Value>,

    /// Data
    pub data: Vec<DownloadDataSource>,

    /// ?
    pub is_vr: bool,

    // ?
    pub success: bool,
}

impl DownloadData {
    /// Get the best source from the download data
    pub fn get_best_source(&self) -> Option<&DownloadDataSource> {
        let mut source_1080p = None;
        let mut source_720p = None;
        let mut source_480p = None;

        for source in self.data.iter() {
            match (source.kind.as_str(), source.label.as_str()) {
                ("mp4", "1080p") => source_1080p = Some(source),
                ("mp4", "720p") => source_720p = Some(source),
                ("mp4", "480p") => source_480p = Some(source),
                _ => {}
            }
        }

        source_1080p.or(source_720p).or(source_480p)
    }
}

/// A source of a video
#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
pub struct DownloadDataSource {
    /// The url
    pub file: Url,

    /// The label
    pub label: String,

    /// The stream type
    #[serde(rename = "type")]
    pub kind: String,
}

impl DownloadDataSource {
    /// Returns true if this is an mp4
    pub fn is_mp4(&self) -> bool {
        self.kind == "mp4"
    }
}
