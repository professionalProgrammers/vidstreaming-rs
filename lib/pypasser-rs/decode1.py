import blackboxprotobuf

SAMPLE_1 = './test_data/795_Request.txt'
SAMPLE_2 = './test_data/147_Request.txt'

with open(SAMPLE_2, 'rb') as file:
	fileContent = file.read()
	message,typedef = blackboxprotobuf.protobuf_to_json(fileContent)
	
	print(message)
	print(typedef)