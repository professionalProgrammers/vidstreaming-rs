# https://github.com/fmoo/python-varint/blob/master/varint.py
def read_var_int(file):
	shift = 0
	final = 0
	
	while True:
		byte = file.read(1)[0]
		final |= (byte & 0x7f) << shift
		shift += 7
		if not (byte & 0x80):
			break
	
	return final
	
def visit_string(file):
	length = read_var_int(file)
	print(f'Length: {length}')
	data = file.read(length)
	print(f'Data: {data}')
	
def visit_group(file):
	while True:
		byte = file.read(1)[0]
		
		wire_type_raw = byte & 0b111
		field_number = byte >> 3
				
		print(f'    Raw Byte: {hex(byte)}')
		print(f'    Wire Type Raw: {wire_type_raw}')
		print(f'    Field Number: {field_number}')
				
		if(wire_type_raw == 0):
			num = read_var_int(file)
			print(f'    Num: {num}')
		elif(wire_type_raw == 1):
			bits_64 = file.read(8)
			print(f'    Bits64: {bits_64}')
		elif(wire_type_raw == 2):
			visit_string(file)
		elif(wire_type_raw == 3):
			visit_group(file)
		elif(wire_type_raw == 5):
			bits_32 = file.read(4)
			print(f'    Bits32: {bits_32}')
		else:
			break
		print()
	

with open("./test_data/795_Request.txt", "rb") as file:	
	while (byte := file.read(1)):
		byte = byte[0]
		
		wire_type_raw = byte & 0b111
		field_number = byte >> 3
		
		print(f'Raw Byte: {hex(byte)}')
		print(f'Wire Type Raw: {wire_type_raw}')
		print(f'Field Number: {field_number}')
		
		if(wire_type_raw == 2):
			visit_string(file)
		elif(wire_type_raw == 3):
			visit_group(file)
			break
		else:
			print(hex(byte))
			break
		print()

