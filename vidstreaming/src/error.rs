// pub type InvalidVidstreamingSourceError = crate::types::vidstreaming_source::FromDocError;

/// Error type
#[derive(thiserror::Error, Debug)]
pub enum Error {
    /// Invalid Url
    #[error(transparent)]
    InvalidUrl(#[from] url::ParseError),

    /// Reqwest HTTP Error
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),

    /// Tokio join error
    #[error(transparent)]
    TokioJoin(#[from] tokio::task::JoinError),

    /// Invalid download data
    #[error("invalid download data")]
    InvalidDownloadData(#[from] crate::types::download_data::FromHtmlError),

    /// Pypasser error
    #[error("pypasser error")]
    Pypasser(#[from] pypasser::Error),

    /// A url is missing a query value
    #[error("url missing query value for '{1}' in '{0}'")]
    UrlMissingQueryValue(String, &'static str),
}

/// A Send ducc::Error
#[derive(thiserror::Error, Debug)]
pub struct DuccError {
    /// Underlying error kind
    #[source]
    pub kind: DuccErrorKind,

    /// Underlying error context
    pub context: Vec<String>,
}

impl std::fmt::Display for DuccError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.kind.fmt(f)
    }
}

impl From<ducc::Error> for DuccError {
    fn from(e: ducc::Error) -> Self {
        Self {
            kind: e.kind.into(),
            context: e.context,
        }
    }
}

/// A Send ducc::ErrorKind
#[derive(thiserror::Error, Debug)]
pub enum DuccErrorKind {
    /// Js conversion error
    #[error("to js conversion error")]
    ToJsConversionError {
        from: &'static str,
        to: &'static str,
    },

    /// From js conversion error
    #[error("from js conversion error")]
    FromJsConversionError {
        from: &'static str,
        to: &'static str,
    },

    /// runtime error
    #[error("runtime error")]
    RuntimeError {
        code: ducc::RuntimeErrorCode,
        name: String,
    },

    /// recursive mut callback
    #[error("recursive mut callback")]
    RecursiveMutCallback,

    /// external error
    #[error("external error")]
    ExternalError,

    /// not a function
    #[error("not a function")]
    NotAFunction,
}

impl From<ducc::ErrorKind> for DuccErrorKind {
    fn from(e: ducc::ErrorKind) -> Self {
        match e {
            ducc::ErrorKind::ToJsConversionError { from, to } => {
                DuccErrorKind::ToJsConversionError { from, to }
            }
            ducc::ErrorKind::FromJsConversionError { from, to } => {
                DuccErrorKind::FromJsConversionError { from, to }
            }
            ducc::ErrorKind::RuntimeError { code, name } => {
                DuccErrorKind::RuntimeError { code, name }
            }
            ducc::ErrorKind::RecursiveMutCallback => DuccErrorKind::RecursiveMutCallback,
            ducc::ErrorKind::ExternalError(_) => DuccErrorKind::ExternalError,
            ducc::ErrorKind::NotAFunction => DuccErrorKind::NotAFunction,
        }
    }
}
