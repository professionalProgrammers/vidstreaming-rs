mod types;

pub use self::types::{
    DownloadData,
    DownloadDataSource,
    DownloadPage,
    VideoPage,
};
use scraper::Html;
use std::sync::Arc;
use url::Url;

/// The error type
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// A reqwest HTTP error
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),

    /// A tokio join failed
    #[error(transparent)]
    TokioJoin(#[from] tokio::task::JoinError),

    /// Invalid VideoPage
    #[error("invalid video page")]
    InvalidVideoPage(#[from] crate::types::video_page::FromHtmlError),

    /// Invalid DownloadPage
    #[error("invalid download page")]
    InvalidDownloadPage(#[from] crate::types::download_page::FromHtmlError),

    /// Url is missing a path
    #[error("url is missing a path")]
    UrlMissingPath,

    /// Missing the video id
    #[error("missing video id")]
    MissingVideoId,
}

/// The client
#[derive(Debug, Clone)]
pub struct Client {
    /// The inner http client
    ///
    /// Probably shouldn't be used by you
    pub client: reqwest::Client,

    /// The api ratelimiter
    pub api_ratelimiter: Arc<tokio::sync::Semaphore>,
}

impl Client {
    /// Make a new Client
    pub fn new() -> Self {
        Self {
            client: reqwest::Client::new(),
            api_ratelimiter: Arc::new(tokio::sync::Semaphore::new(4)),
        }
    }

    /// get html from a page
    async fn get_html<T, F>(&self, url: &str, func: F) -> Result<T, Error>
    where
        F: FnOnce(Html) -> T + Send + 'static,
        T: Send + 'static,
    {
        let text = self
            .client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .text()
            .await?;
        Ok(tokio::task::spawn_blocking(move || {
            let html = Html::parse_document(&text);
            func(html)
        })
        .await?)
    }

    /// Get the video page
    pub async fn get_video_page(&self, url: &str) -> Result<VideoPage, Error> {
        Ok(self
            .get_html(url, |html| VideoPage::from_html(&html))
            .await??)
    }

    /// Get the download page
    pub async fn get_download_page(&self, url: &str) -> Result<DownloadPage, Error> {
        Ok(self
            .get_html(url, |html| DownloadPage::from_html(&html))
            .await??)
    }

    /// Get the downloadData for a given video by id
    pub async fn get_download_data(&self, id: &str) -> Result<DownloadData, Error> {
        let url = format!("https://fembed-hd.com/api/source/{id}");
        let _permit = self
            .api_ratelimiter
            .acquire()
            .await
            .expect("semaphore closed");
        Ok(self
            .client
            .post(url)
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?)
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}

/// Extract a video id from a url
pub fn extract_video_id(url: &Url) -> Result<&str, Error> {
    let mut path_iter = url.path_segments().ok_or(Error::UrlMissingPath)?;

    let _url_type_specifier = path_iter.next().ok_or(Error::MissingVideoId)?;

    path_iter.next().ok_or(Error::MissingVideoId)
}

/// Transform a video page url into a download page url
pub fn make_download_url(url: &Url) -> Result<Url, Error> {
    let video_id = extract_video_id(url)?;

    let mut download_page_url = url.clone();

    {
        let mut path_segments_mut = download_page_url
            .path_segments_mut()
            .map_err(|()| Error::UrlMissingPath)?;

        path_segments_mut.clear().push("f").push(video_id);
    }

    Ok(download_page_url)
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn it_works() {
        let url = Url::parse("https://fembed-hd.com/v/l364xun6l7dl3jw").expect("invalid video url");
        let client = Client::new();

        let video_page = client
            .get_video_page(url.as_str())
            .await
            .expect("failed to get video page");

        dbg!(&video_page);

        let download_page_url = make_download_url(&url).expect("failed to make a download url");

        let download_page = client
            .get_download_page(download_page_url.as_str())
            .await
            .expect("failed to get download page");

        dbg!(&download_page);

        let video_id = extract_video_id(&url).expect("failed to extract video id");

        let download_data = client
            .get_download_data(video_id)
            .await
            .expect("failed to get download data");

        dbg!(&download_data);

        let source = download_data
            .get_best_source()
            .expect("failed to select a source");

        dbg!(&source);

        let head_request = client
            .client
            .head(source.file.as_str())
            .send()
            .await
            .expect("failed to send HEAD request");

        dbg!(head_request.url());
    }
}
