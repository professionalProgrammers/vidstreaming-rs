use crate::{
    DownloadData,
    Error,
    LinkData,
};
use scraper::Html;
use url::Url;

/// Client
#[derive(Clone)]
pub struct Client {
    /// The inner http client.
    ///
    /// Probably shouldn't be used by you
    pub client: reqwest::Client,

    pypasser_client: pypasser::Client,

    pub vidstreaming_client: vidstreaming_new::Client,
}

impl Client {
    /// Create a new [`Client`].
    pub fn new() -> Self {
        let mut header_map = reqwest::header::HeaderMap::new();
        header_map.insert(
            reqwest::header::ACCEPT,
            "*/*".parse().expect("invalid header"),
        );
        header_map.insert(
            reqwest::header::ACCEPT_LANGUAGE,
            "en-US,en;q=0.9".parse().expect("invalid header"),
        );
        header_map.insert(
            reqwest::header::CONNECTION,
            "Keep-Alive".parse().expect("invalid header"),
        );

        // Build custom reqwest client
        let client = reqwest::Client::builder()
            .cookie_store(true)
            .default_headers(header_map)
            .user_agent(crate::USER_AGENT_STR)
            .build()
            .expect("failed to build vidstreaming client");

        // Pass client to pypasser
        let pypasser_client = pypasser::Client::from_client(client.clone());

        Self {
            client,
            pypasser_client,

            vidstreaming_client: vidstreaming_new::Client::new(),
        }
    }

    /// Get a url as text
    async fn get_text(&self, url: &str) -> Result<String, Error> {
        Ok(self
            .client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .text()
            .await?)
    }

    /// Get the url as html, then transform it
    async fn get_html<F, T>(&self, url: &str, transform: F) -> Result<T, Error>
    where
        F: FnOnce(Html) -> T + Send + 'static,
        T: Send + 'static,
    {
        let text = self.get_text(url).await?;
        Ok(tokio::task::spawn_blocking(move || {
            let html = Html::parse_document(&text);
            transform(html)
        })
        .await?)
    }

    /// Get the download data for a url
    pub async fn get_download_data<U>(&self, url: U) -> Result<DownloadData, Error>
    where
        U: TryInto<Url, Error = url::ParseError>,
    {
        let url = url.try_into()?;

        // Get id from url
        let id = url
            .query_pairs()
            .find_map(|(k, v)| if k == "id" { Some(v) } else { None })
            .ok_or_else(|| {
                Error::UrlMissingQueryValue(url.query().unwrap_or("").to_string(), "id")
            })?;

        // Get the download data page.
        // We will populate the links field in the next few requests.
        let mut download_data = self
            .get_html(url.as_str(), |html| DownloadData::from_html(&html))
            .await??;

        // Solve the captcha
        // TODO: determine anchor url from page
        let anchor_url = "https://www.google.com/recaptcha/api2/anchor?ar=1&k=6LealdkbAAAAAHbox4XlHS8ZMQ6lkcx96WV62UfO&co=aHR0cHM6Ly9nb2dvcGxheS5pbzo0NDM.&hl=en&v=TDBxTlSsKAUm3tSIa0fwIqNu&size=invisible&cb=h90upob7cm2x";
        let recaptcha_response = self.pypasser_client.recaptcha_v3(anchor_url).await?;

        // Get the links and parse them
        let text = self
            .client
            .post(crate::DOWNLOAD_URL)
            .header(reqwest::header::REFERER, url.as_str())
            .header(reqwest::header::HOST, "gogoplay.io")
            .header(reqwest::header::ORIGIN, "https://gogoplay.io")
            .header("X-Requested-With", "XMLHttpRequest")
            .form(&[("captcha_v3", recaptcha_response.as_str()), ("id", &*id)])
            .send()
            .await?
            .error_for_status()?
            .text()
            .await?;
        let link_data = tokio::task::spawn_blocking(move || {
            let html = Html::parse_fragment(&text);
            LinkData::from_html(&html)
        })
        .await??;

        // Populate download data
        download_data.link_data = link_data;

        Ok(download_data)
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    /*
    #[tokio::test]
    #[ignore]
    async fn get_vidstreaming_source() {
        let client = Client::new();
        // TODO: Looks like they added more indirection. Find out if this matters.
        // Right now, the cli works so this is a low priority.
        // This link also expires frequently
        // let url = "https://gogo-stream.com/loadserver.php?id=MTQ3ODY3";
        let url = "https://streamani.net/embedplus?id=MTYyNTc3&token=-96vMbucXhlI-6s6lRq3sA&expires=1627969037";

        let _source = client
            .get_vidstreaming_source(url)
            .await
            .expect("failed to get vidstreaming source");
    }
    */

    // ReCaptcha bypass is very spotty and almost unusable
    #[tokio::test]
    #[ignore]
    async fn download_data_works() {
        let client = Client::new();
        let url = "https://gogoplay.io/download?id=MTc4MjMw&title=Princess+Lover%21+Maiden%27s+Secret&typesub=SUB&sub=&cover=Y292ZXIvcHJpbmNlc3MtbG92ZXItbWFpZGVucy1zZWNyZXQucG5n&mip=0.0.0.0&refer=https://gogoplay1.com/&ch=d41d8cd98f00b204e9800998ecf8427e";
        let download_data = client
            .get_download_data(url)
            .await
            .expect("failed to get download data");

        dbg!(download_data);
    }
}
