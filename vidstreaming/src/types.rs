pub mod download_data;
// pub mod vidstreaming_source;

pub use self::download_data::{
    DownloadData,
    LinkData,
};

/*
    episode::{
        Episode,
        RelatedEpisode,
    },
*/

/*
vidstreaming_source::{
        StreamSource,
        StreamSourceKind,
        VidstreamingSource,
    },
*/
