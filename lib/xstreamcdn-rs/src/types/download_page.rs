use scraper::Html;

/// error that may occur while parsing a DownloadPage
#[derive(Debug, thiserror::Error)]
pub enum FromHtmlError {}

/// A download page
#[derive(Debug)]
pub struct DownloadPage {}

impl DownloadPage {
    /// Make a DownloadPage from Html
    pub(crate) fn from_html(_html: &Html) -> Result<Self, FromHtmlError> {
        Ok(Self {})
    }
}
