use std::path::Path;

/// Clean the path.
pub(crate) fn clean_path(input: &str) -> String {
    input
        .chars()
        .map(|c| match c {
            ':' | '?' => '-',
            c => c,
        })
        .collect()
}

/// Try to see if a file exists
pub async fn try_exists<P: AsRef<Path>>(path: P) -> std::io::Result<bool> {
    let metadata = tokio::fs::metadata(path).await;
    match metadata {
        Ok(_metadata) => Ok(true),
        Err(error) if error.kind() == std::io::ErrorKind::NotFound => Ok(false),
        Err(error) => Err(error),
    }
}
