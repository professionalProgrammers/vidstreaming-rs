use once_cell::sync::Lazy;
use scraper::{
    Html,
    Selector,
};
use url::Url;

// HTML selectors
static TITLE_SELECTOR: Lazy<Selector> =
    Lazy::new(|| Selector::parse("title").expect("invalid title selector"));
static MIRRIOR_LINK_SELECTOR: Lazy<Selector> =
    Lazy::new(|| Selector::parse(".mirror_link").expect("invalid mirror link selector"));
static A_SELECTOR: Lazy<Selector> = Lazy::new(|| Selector::parse("a").expect("invalid a selector"));

/// An error that may occur while parsing [`DownloadData`] from html.
#[derive(Debug, thiserror::Error)]
pub enum FromHtmlError {
    /// Missing the title
    #[error("missing title")]
    MissingTitle,

    /// A link was invalid
    #[error("invalid link")]
    InvalidLink(#[source] url::ParseError),

    /// Missing main links
    #[error("missing main links")]
    MissingMainLinks,

    /// Missing mirror links
    #[error("missing mirror links")]
    MissingMirrorLinks,
}

/// Download data
#[derive(Debug, PartialEq, Clone)]
pub struct DownloadData {
    /// The name of the download
    pub name: String,

    /// Link data
    pub link_data: LinkData,
}

impl DownloadData {
    /// Create a [`DownloadData`] page from [`Html`].
    pub(crate) fn from_html(html: &Html) -> Result<Self, FromHtmlError> {
        // Get the title
        let name = html
            .select(&TITLE_SELECTOR)
            .next()
            .and_then(|el| el.text().next())
            .ok_or(FromHtmlError::MissingTitle)?
            .to_string();

        Ok(Self {
            name,

            // This is empty as we need a web request to get it
            link_data: LinkData {
                main_links: Vec::new(),
                mirror_links: Vec::new(),
            },
        })
    }

    /// Iter over the src
    pub fn get_src_iter(&self) -> impl Iterator<Item = &Url> {
        self.link_data.main_links.iter().rev()
    }
}

/// Link data for a downloads page
#[derive(Debug, PartialEq, Clone)]
pub struct LinkData {
    /// Main links
    pub main_links: Vec<Url>,

    /// Link Mirrors
    pub mirror_links: Vec<Url>,
}

impl LinkData {
    /// Create a [`LinkData`] page from [`Html`].
    pub(crate) fn from_html(html: &Html) -> Result<Self, FromHtmlError> {
        let mut link_iter = html.select(&MIRRIOR_LINK_SELECTOR);

        let main_links = link_iter
            .next()
            .ok_or(FromHtmlError::MissingMainLinks)?
            .select(&A_SELECTOR)
            .filter_map(|el| el.value().attr("href"))
            .map(Url::parse)
            .collect::<Result<Vec<_>, _>>()
            .map_err(FromHtmlError::InvalidLink)?;

        let mirror_links = link_iter
            .next()
            .ok_or(FromHtmlError::MissingMirrorLinks)?
            .select(&A_SELECTOR)
            .filter_map(|el| el.value().attr("href"))
            .map(Url::parse)
            .collect::<Result<Vec<_>, _>>()
            .map_err(FromHtmlError::InvalidLink)?;

        Ok(Self {
            main_links,
            mirror_links,
        })
    }
}
