use scraper::Html;

/// Error that may occur while creating a VideoPage from html
#[derive(Debug, thiserror::Error)]
pub enum FromHtmlError {}

/// The video page
#[derive(Debug)]
pub struct VideoPage {}

impl VideoPage {
    /// Create a page from html
    pub(crate) fn from_html(_html: &Html) -> Result<Self, FromHtmlError> {
        Ok(Self {})
    }
}
