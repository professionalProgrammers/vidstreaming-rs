#[allow(clippy::uninlined_format_args)]
mod commands;
mod util;

use anyhow::Context;

#[derive(argh::FromArgs)]
#[argh(description = "A tool to download files from vidstreaming.io")]
pub struct Args {
    #[argh(subcommand)]
    subcommand: Subcommand,
}

#[derive(argh::FromArgs, PartialEq, Debug)]
#[argh(subcommand)]
enum Subcommand {
    Download(self::commands::download::Args),
}

fn main() -> anyhow::Result<()> {
    let args = argh::from_env();
    real_main(args)
}

fn real_main(args: Args) -> anyhow::Result<()> {
    let client = vidstreaming::Client::new();
    let xstreamcdn_client = xstreamcdn::Client::new();

    let rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .context("failed to start tokio runtime")?;

    rt.block_on(async_main(&client, &xstreamcdn_client, args))?;

    Ok(())
}

async fn async_main(
    client: &vidstreaming::Client,
    xstreamcdn_client: &xstreamcdn::Client,
    args: Args,
) -> anyhow::Result<()> {
    match args.subcommand {
        Subcommand::Download(args) => {
            self::commands::download::exec(client, xstreamcdn_client, args).await
        }
    }
}
