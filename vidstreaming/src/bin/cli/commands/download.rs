use crate::util::clean_path;
use anyhow::{
    bail,
    Context,
};
use indicatif::ProgressBar;
use std::path::{
    Path,
    PathBuf,
};
use tokio::{
    fs::File,
    io::{
        AsyncWriteExt,
        BufWriter,
    },
    process::Command,
};
use url::Url;

#[derive(argh::FromArgs, PartialEq, Debug)]
#[argh(subcommand, name = "download", description = "Download an episode")]
pub struct Args {
    #[argh(positional, description = "the url of the video to download")]
    pub url: String,

    #[argh(
        option,
        description = "the output file path",
        short = 'o',
        long = "output"
    )]
    pub output: Option<String>,

    #[argh(
        switch,
        description = "use the series downloader",
        short = 's',
        long = "series"
    )]
    pub series: bool,

    #[argh(
        switch,
        description = "only use vidstreaming sources",
        long = "only-vidstreaming"
    )]
    pub only_vidstreaming: bool,

    #[argh(
        option,
        description = "the episodes to download. If not specified, all are downloaded."
    )]
    pub episodes: Vec<usize>,

    #[argh(switch, long = "dry-run", description = "whether to make this dry run")]
    pub dry_run: bool,
}

/*
let download_data_url = episode
    .get_download_data_url()
    .context("failed to get download data url")?;

let download_data = client
    .get_download_data(download_data_url.as_str())
    .await
    .context("failed to get download data")?;

let url = download_data
    .link_data
    .main_links
    .first()
    .context("missing main link source")?
    .clone();
*/

/*
pub struct DownloadManager {
    client: vidstreaming::Client,
}

impl DownloadManager {
    /// Make a new download manager
    pub fn new(client: vidstreaming::Client) -> Self {
        Self { client }
    }

    /// Start a download for an episode url.
    ///
    /// # Arguments
    /// `episode_url`: the episode url to download
    /// `episode_name`: The episode name
    /// `out_path`: An optional dir to download to.
    pub async fn start_download(
        &self,
        episode_url: &Url,
        episode_name: &str,
        out_path: Option<&Path>,
        is_series: bool,
    ) -> anyhow::Result<()> {
        let out_path = match out_path {
            Some(out_path) => {
                if is_series {
                    bail!("cannot use `out_path` with a series download");
                } else {
                    PathBuf::from(out_path)
                }
            }
            None => {
                // If the user has not requested a specific filename,
                // just assume we can name it whatever.
                if is_series {
                    // If this is a series download, we want the output to be in a directory with the anime name.
                    // Get the episode slug, trim the episode-n part, and set the dir name.

                    // Get the anime/episode name part of the url
                    let episode_url_slug = episode_url
                        .path_segments()
                        .context("missing path in url")?
                        .last()
                        .context("url has no path segments")?;

                    // Trim the non-anime name bits
                    let anime_title = episode_url_slug
                        .trim_end_matches(char::is_numeric)
                        .trim_end_matches('-')
                        .trim_end_matches("episode")
                        .trim_end_matches('-');

                    let mut path = PathBuf::from(clean_path(anime_title));
                    path.push(clean_path(episode_name));
                    path.set_extension("mp4");
                    path
                } else {
                    // This is a single download.
                    // Let's set the output file name to the anime name.
                    PathBuf::from(clean_path(episode_name))
                }
            }
        };

        let file_exists = crate::util::try_exists(out_path)
            .await
            .context("failed to check if file exists")?;

        // If this is a series mode, we just need the related episodes
        if is_series {
            // Get episode page
            let episode = self
                .client
                .get_episode(episode_url.as_str())
                .await
                .context("failed to get epsiode page")?;
        }

        if file_exists {
            return Ok(());
        }

        Ok(())
    }
}
*/

/// The data created by starting a download
pub struct StartDownloadData {
    /// The number of the epsiode.
    ///
    /// Starts at 0.
    pub episode_number: usize,

    /// The output file path
    pub out_path: PathBuf,

    /// The result
    pub result: anyhow::Result<Option<VideoSource>>,
}

/// The selected video player source
#[derive(Debug)]
pub enum VideoSource {
    /// from vidstreaming directly
    Vidstreaming {
        /// The video source
        video_source: vidstreaming_new::VideoDataSource,

        /// The referer url
        referer_url: Url,
    },
    /// XStreamCdn
    XStreamCdn {
        /// The video source
        source: xstreamcdn::DownloadDataSource,
    },
}

/// Start a series download
pub async fn perform_series_download(
    client: &vidstreaming::Client,
    xstreamcdn_client: &xstreamcdn::Client,
    episode_url: &Url,
    output_directory: Option<&Path>,
    only_vidstreaming: bool,
    dry_run: bool,
    episodes: &[usize],
) -> anyhow::Result<()> {
    // If the user provided an output directory, use that.
    // If not, make one from the anime name
    let output_directory = match output_directory {
        Some(path) => PathBuf::from(path),
        None => {
            // Get the anime/episode name part of the url
            let episode_url_slug = episode_url
                .path_segments()
                .context("missing path in url")?
                .last()
                .context("url has no path segments")?;

            // Trim the non-anime name bits
            let anime_title = episode_url_slug
                .trim_end_matches(char::is_numeric)
                .trim_end_matches('-')
                .trim_end_matches("episode")
                .trim_end_matches('-');

            // Clean path and make a path
            PathBuf::from(clean_path(anime_title))
        }
    };

    // Ensure output directory exists
    tokio::fs::create_dir_all(&output_directory)
        .await
        .context("failed to create output directory")?;

    eprintln!("Downloading series to '{}'...", output_directory.display());

    // Get the episode
    let episode = client
        .vidstreaming_client
        .get_episode(episode_url.as_str())
        .await
        .context("failed to get epsiode page")?;

    // Get the number of related episodes
    let num_related_episodes = episode.related_episodes.len();

    // Spawn child tasks
    let (tx, mut rx) = tokio::sync::mpsc::channel(num_related_episodes);
    for (i, related_episode) in episode.related_episodes.iter().enumerate() {
        if !episodes.is_empty() && !episodes.contains(&(i + 1)) {
            continue;
        }

        // Copy needed state for child task
        let client = client.clone();
        let xstreamcdn_client = xstreamcdn_client.clone();
        let episode_url = related_episode.url.as_str().to_string();
        let tx = tx.clone();
        let output_directory = output_directory.clone();
        let filename = {
            let mut filename = clean_path(&related_episode.name);
            // Related episode names differ from episode names. We fix that here.
            if related_episode.anime_type.is_sub() {
                filename.push_str(" English Subbed");
            }
            filename.push_str(".mp4");
            filename
        };
        let out_path = output_directory.join(filename);

        tokio::spawn(async move {
            let result: anyhow::Result<_, _> = async {
                let file_exists = crate::util::try_exists(&out_path)
                    .await
                    .context("failed to check if file exists")?;

                // If file exists, exit
                if file_exists {
                    return Ok(None);
                }

                // If dry run, exit
                if dry_run {
                    return Ok(None);
                }

                // Get episode
                let episode = client
                    .vidstreaming_client
                    .get_episode(&episode_url)
                    .await
                    .with_context(|| format!("failed to get episode {}", i + 1))?;

                // Get video player page
                let video_player = client
                    .vidstreaming_client
                    .get_video_player(episode.video_player_url.as_str())
                    .await
                    .context("failed to get video player")?;

                // Give a better error if we have no sources
                if video_player.sources.is_empty() {
                    bail!("video player has no sources");
                }

                if !only_vidstreaming {
                    // Try the xstreamcdn source first as it seems more reliable
                    let xstreamcdn_url = video_player
                        .sources
                        .iter()
                        .find(|url| url.host() == Some(url::Host::Domain("fembed-hd.com")));

                    if let Some(xstreamcdn_url) = xstreamcdn_url {
                        let video_id = xstreamcdn::extract_video_id(xstreamcdn_url)
                            .context("missing xstreamcdn video id")?;
                        let download_data = xstreamcdn_client
                            .get_download_data(video_id)
                            .await
                            .context("failed to get xstreamcdn download data")?;
                        let source = download_data
                            .get_best_source()
                            .context("failed to get best xstreamcdn source")?
                            .clone();

                        return Ok(Some(VideoSource::XStreamCdn { source }));
                    }
                }

                // TODO: Put behind flag
                // eprintln!("Located vidstreaming sources: {:#?}", video_player.video_data.source);

                let video_data = client
                    .vidstreaming_client
                    .get_video_player_video_data(&video_player)
                    .await
                    .context("failed to get video player")?;

                // Get the best video source
                let video_source = video_data
                    .get_best_source()
                    .with_context(|| {
                        format!(
                            "failed to select a vidstreaming source, got sources {:#?}",
                            video_data.source
                        )
                    })?
                    .clone();

                Ok(Some(VideoSource::Vidstreaming {
                    video_source,
                    referer_url: episode.video_player_url,
                }))
            }
            .await;

            // Send response
            let _ = tx
                .send(StartDownloadData {
                    episode_number: i,
                    out_path,
                    result,
                })
                .await
                .is_ok();
        });
    }
    drop(tx);

    // TODO: Parallelize downloads?
    // It might be worth it to optimize downloading individual files rather than paralleizing.
    //
    // Download files as data comes in
    let mut num_successful_downloads = 0;
    while let Some(episode_result) = rx.recv().await {
        let result = async {
            let _episode_number = episode_result.episode_number;
            let out_path = episode_result.out_path;
            let source = episode_result.result?;

            eprintln!(
                "Downloading '{}' ({}/{})...",
                out_path.display(),
                num_successful_downloads + 1,
                num_related_episodes
            );

            // Source url is not found if it was confirmed downloaded earlier
            let video_source = match source {
                Some(source) => source,
                None => {
                    eprintln!("'{}' exists, skipping...", out_path.display());
                    return Ok(());
                }
            };

            match video_source {
                VideoSource::Vidstreaming {
                    video_source,
                    referer_url,
                } => {
                    eprintln!("Using vidstreaming source...");
                    if video_source.is_mp4() {
                        download_mp4(
                            client,
                            &out_path,
                            video_source.file.as_str(),
                            Some(referer_url.as_str()),
                        )
                        .await
                        .context("failed to download")?;
                    } else if video_source.is_hls() {
                        download_ffmpeg(&out_path, video_source.file.as_str())
                            .await
                            .context("failed to download")?;
                    } else {
                        bail!("unknown source type '{}'", video_source.kind);
                    }
                }
                VideoSource::XStreamCdn { source } => {
                    eprintln!("Using xstreamcdn source...");
                    if source.is_mp4() {
                        download_mp4(client, &out_path, source.file.as_str(), None)
                            .await
                            .context("failed to download")?;
                    } else {
                        bail!("unknown source type '{}'", source.kind);
                    }
                }
            }

            Ok(())
        }
        .await;

        match result {
            Ok(()) => {
                num_successful_downloads += 1;
            }
            Err(e) => {
                eprintln!("Error: {:?}", e);
            }
        }
    }

    Ok(())
}

pub async fn exec(
    client: &vidstreaming::Client,
    xstreamcdn_client: &xstreamcdn::Client,
    args: Args,
) -> anyhow::Result<()> {
    // Start to process input
    let url: Url = args.url.as_str().parse().context("invalid url")?;
    let mut output = args.output;

    // If it is a series download, perform it
    if args.series {
        perform_series_download(
            client,
            xstreamcdn_client,
            &url,
            output.as_deref().map(|path| path.as_ref()),
            args.only_vidstreaming,
            args.dry_run,
            &args.episodes,
        )
        .await
        .context("failed to perform series download")?;
        return Ok(());
    }

    // Get episode page
    let episode = client
        .vidstreaming_client
        .get_episode(url.as_str())
        .await
        .context("failed to get epsiode page")?;

    // If the user has not requested a specific filename,
    // just assume we can name it whatever.
    if output.is_none() {
        // let's use something more descriptive than "video.mp4".
        let filename = clean_path(&episode.name);
        output = Some(format!("{}.mp4", filename));
    }

    // Get a video source from the video player's list
    let video_player = client
        .vidstreaming_client
        .get_video_player(episode.video_player_url.as_str())
        .await
        .context("failed to get video player")?;

    if video_player.sources.is_empty() {
        bail!("video player has no sources");
    }

    let video_data = client
        .vidstreaming_client
        .get_video_player_video_data(&video_player)
        .await
        .context("failed to get video player")?;

    let source = video_data
        .get_best_source()
        .context("failed to select a source")?
        .clone();

    let episode_url = episode.video_player_url.clone();

    let filename = output.unwrap_or_else(|| "video.mp4".to_string());
    println!("Downloading '{}'...", filename);

    download_mp4(
        client,
        filename.as_ref(),
        source.file.as_str(),
        Some(episode_url.as_str()),
    )
    .await
    .context("failed to download")?;

    /*
    download_vidstreaming_source_cli(client, url.as_str(), filename.as_ref())
        .await
        .context("failed to download")?;
    */

    Ok(())
}

/*
pub async fn download_vidstreaming_source_cli(
    client: &vidstreaming::Client,
    url: &str,
    filename: &Path,
) -> anyhow::Result<()> {
    let vidstreaming_source = client
        .get_vidstreaming_source(url)
        .await
        .context("failed to get vidstreaming source")?;
    let stream_source = vidstreaming_source
        .get_default_src()
        .context("failed to extract valid stream source")?;

    match &stream_source.kind {
        StreamSourceKind::Mp4 => {
            download_mp4(client, filename, stream_source.file.as_str())
                .await
                .context("failed to download mp4")?;
        }
        StreamSourceKind::Hls => {
            download_ffmpeg(filename, stream_source)
                .await
                .context("failed to download HLS stream")?;
        }
        StreamSourceKind::Other(source) => {
            // Lets try ffmpeg anyways
            eprintln!("Unknown source type '{}'", source);
            eprintln!("Source Url: {}", &stream_source.file);
            eprintln!("Attempting to use FFmpeg...");
            download_ffmpeg(filename, stream_source)
                .await
                .context("failed to download hls stream")?;
        }
    }

    println!("Done.");

    Ok(())
}
*/

async fn download_mp4(
    client: &vidstreaming::Client,
    filename: &Path,
    stream_source_url: &str,
    referer_url: Option<&str>,
) -> anyhow::Result<()> {
    let mut out_file = File::create(&filename)
        .await
        .map(BufWriter::new)
        .with_context(|| format!("failed to open file '{}'", filename.display()))?;

    let result: anyhow::Result<()> =
        async {
            let mut request = client.client.get(stream_source_url);

            if let Some(referer_url) = referer_url {
                request = request.header(reqwest::header::REFERER, referer_url);
            }

            let mut download = request
                .send()
                .await
                .context("failed to start download")?
                .error_for_status()?;

            let total = download.content_length();

            let progress = setup_progress_bar(total);

            while let Some(chunk) = download.chunk().await.context("failed to get next chunk")? {
                if let Err(e) = out_file.write_all(&chunk).await.with_context(|| {
                    format!("failed to write chunk to file '{}'", filename.display())
                }) {
                    progress.finish();
                    return Err(e);
                }

                progress.inc(chunk.len() as u64);
            }

            out_file.flush().await?;
            out_file.get_ref().sync_all().await?;
            progress.finish();

            Ok(())
        }
        .await;

    if let Err(e) = result {
        let _ = tokio::fs::remove_file(filename).await.is_ok();
        return Err(e);
    }

    println!("Done.");

    Ok(())
}

fn setup_progress_bar(total: Option<u64>) -> ProgressBar {
    let total = match total {
        Some(total) => total,
        None => {
            return ProgressBar::hidden();
        }
    };

    let progress = ProgressBar::new(total);
    progress.set_style(indicatif::ProgressStyle::default_bar().template("[Time = {elapsed_precise} | ETA = {eta_precise} | Speed = {bytes_per_sec}] {wide_bar} {bytes}/{total_bytes}"));
    progress
}

async fn download_ffmpeg(filename: &Path, url: &str) -> anyhow::Result<()> {
    println!("Using ffmpeg to convert stream to mp4...");
    let status = Command::new("ffmpeg")
        .args(["-i", url, "-acodec", "copy", "-vcodec", "copy"])
        .arg(filename)
        .stdout(std::process::Stdio::inherit())
        .stderr(std::process::Stdio::inherit())
        .stdin(std::process::Stdio::null())
        .status()
        .await
        .context("failed to run FFmpeg")?;

    if !status.success() {
        eprintln!("FFmpeg exited with a non-zero error: {}", status);
    }

    Ok(())
}
