pub mod client;
pub mod error;
pub mod types;

pub use crate::{
    client::Client,
    error::{
        DuccError,
        Error,
    },
    types::{
        DownloadData,
        LinkData,
    },
};

pub use vidstreaming_new::AnimeType;

/*
 StreamSource,
        StreamSourceKind,
        VidstreamingSource,
*/

pub(crate) const DOWNLOAD_URL: &str = "https://gogoplay.io/download";
pub(crate) const USER_AGENT_STR: &str =
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5015.0 Safari/537.36";
